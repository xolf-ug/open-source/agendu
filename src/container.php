<?php
/**
 * @param $c
 * @return \Agendu\Files\Handler
 */
$container['files'] = function ($c) {
    $handler = new \Agendu\Files\Handler($c['settings']['files']);
    return $handler;
};

/**
 * @param $c
 * @return \Monolog\Logger
 */
$container['logger'] = function ($c) {
    $logger = new \Monolog\Logger('agendu');
    $file_handler = new \Monolog\Handler\StreamHandler($c['files']->getPathForFile('log/info.log'), Monolog\Logger::INFO);
    $logger->pushHandler($file_handler);
    $file_handler = new \Monolog\Handler\StreamHandler($c['files']->getPathForFile('log/error.log'), Monolog\Logger::ERROR);
    $logger->pushHandler($file_handler);

    return $logger;
};

$container['sql'] = function ($c) {
    $sql = new Agendu\OwnPDO('sqlite:'.$c['files']->prepareFile('agendu.sqlite3'));
    $sql->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

    $migrations = new \Agendu\Migrations\Executor($sql);
    $migrations->initDatabase();

    return $sql;
};

// Register component on container
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(__DIR__.'/view/templates', [
        //'cache' => $c['files']->getPathForFile('twig-cache')
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

    return $view;
};
