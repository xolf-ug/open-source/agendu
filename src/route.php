<?php
use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

$app->get('/', function (Request $request, Response $response) {

    $dashboard = new \Agendu\Dashboard($this->sql, $this->files);
    if($dashboard->installationRequired) {
        return $response->withRedirect($this->router->pathFor('setup', ['step' => 1]));
    }

    $response->getBody()->write($this->files->prepareFile('hello.txt'));

    return $response;
})->setName('dashboard');

$app->get('/setup/{step}', function (Request $request, Response $response) {
    $setup = new \Agendu\Setup($this->sql);



    return $this->view->render($response, "setup.twig", ["step" => $request->getAttribute('step')]);
})->setName('setup');

$app->get('/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $this->logger->info("Hello, $name");
    $this->logger->error("Error with $name!");
    $this->sql->getAttribute(PDO::ATTR_ERRMODE);
    $response->getBody()->write($this->files->prepareFile('hello.txt'));

    return $response;
});
