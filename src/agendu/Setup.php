<?php

namespace Agendu;

class Setup
{

    /**
     * @var OwnPDO
     */
    private $sql;

    public function __construct(OwnPDO $sql)
    {

    }

    /**
     * @return OwnPDO
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param OwnPDO $sql
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
    }

}