<?php

namespace Agendu\Files;

use Agendu\Files\Exceptions\HandlerException;

class Handler
{

    /**
     * @var string
     */
    protected $filesDirectory;

    public function __construct ($fileDirectory)
    {

        if(is_dir($fileDirectory)) {
            $this->setFilesDirectory($fileDirectory);
        } else {
            throw new HandlerException($fileDirectory.' is not a directory');
        }
    }

    /**
     * @param $file
     * @return bool
     */
    public function exists($file)
    {
        return file_exists($this->getPathForFile($file));
    }

    /**
     * @param $file
     * @return string
     * @throws HandlerException
     */
    public function getPathForFile ($file)
    {
        $this->validatePath($file);
        return $this->getFilesDirectory().'/'.$file;
    }

    /**
     * @param $file
     * @return string
     */
    public function prepareFile ($file)
    {
        $this->validatePath($file);
        $file = $this->getPathForFile($file);

        if(!file_exists($file)) {
            fclose(fopen($file, 'w'));
        }

        return $file;
    }

    /**
     * @param array $files
     * @return array
     */
    public function getPathsForFiles (array $files)
    {
        $paths = [];
        foreach ($files as $file) {
            $paths[] = $this->getPathForFile($file);
        }

        return $paths;
    }

    /**
     * @param array $files
     * @return array
     */
    public function prepareFiles (array $files)
    {
        $paths = [];
        foreach ($files as $file) {
            $paths[] = $this->prepareFile($file);
        }

        return $paths;
    }

    private function validatePath ($path)
    {
        if(!is_string($path)) {
            throw new HandlerException('Given path is not a string '.var_export($path, true));
        }
    }

    /**
     * @return string
     */
    public function getFilesDirectory ()
    {
        return $this->filesDirectory;
    }

    /**
     * @param string $filesDirectory
     */
    public function setFilesDirectory ($filesDirectory)
    {
        $this->filesDirectory = $filesDirectory;
    }

}
