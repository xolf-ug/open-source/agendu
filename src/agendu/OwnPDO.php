<?php

namespace Agendu;

class OwnPDO extends \PDO
{

    /**
     * @param $query
     * @return array
     */
    public function fetchAssocByQuery($query)
    {
        $statement = $this->query($query);
        return $statement->fetchAll(self::FETCH_ASSOC);
    }


}
