<?php

namespace Agendu;

use Agendu\Files\Handler;

class Dashboard
{

    /**
     * @var OwnPDO
     */
    private $sql;

    /**
     * @var Handler
     */
    private $fileHandler;

    /**
     * @var bool
     */
    public $installationRequired = false;

    public function __construct(OwnPDO $sql, Handler $fileHandler)
    {
        $this->setSql($sql);
        $this->setFileHandler($fileHandler);

        if(count($this->getSql()->fetchAssocByQuery("SELECT id FROM user WHERE 1=1")) == 0) {
            $this->installationRequired = true;
        }

    }

    /**
     * @return OwnPDO
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param OwnPDO $sql
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
    }

    /**
     * @return Handler
     */
    public function getFileHandler()
    {
        return $this->fileHandler;
    }

    /**
     * @param Handler $fileHandler
     */
    public function setFileHandler($fileHandler)
    {
        $this->fileHandler = $fileHandler;
    }

}