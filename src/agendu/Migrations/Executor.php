<?php

namespace Agendu\Migrations;

use Agendu\OwnPDO;

class Executor
{

    /**
     * @var OwnPDO
     */
    private $sql;

    public function __construct (OwnPDO $sql)
    {
        $this->setSql($sql);
    }

    public function initDatabase ()
    {

        // user Table
        $this->getSql()->exec("CREATE TABLE IF NOT EXISTS `user` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL,
	`person`	INTEGER,
	`gender`	TEXT,
	`street`	TEXT NOT NULL,
	`house_number`	TEXT NOT NULL,
	`zip`	INTEGER NOT NULL,
	`city`	TEXT NOT NULL,
	`country`	TEXT NOT NULL,
	`email`	TEXT,
	`phone`	TEXT,
	`fax`	TEXT,
	`website`	TEXT
	`created`	INTEGER
);");

        // clients Table
        $this->getSql()->exec("CREATE TABLE IF NOT EXISTS `clients` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`user_id`	INTEGER NOT NULL UNIQUE,
	`joined`	INTEGER NOT NULL,
	`outstanding`	NUMERIC
);");
        // team Table
        $this->getSql()->exec("CREATE TABLE IF NOT EXISTS `team` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`user_id`	INTEGER NOT NULL UNIQUE,
	`joined`	INTEGER NOT NULL,
	`permission`	INTEGER
);");
        // settings Table
        $this->getSql()->exec("CREATE TABLE IF NOT EXISTS `settings` (
	`setting`	TEXT NOT NULL UNIQUE,
	`value`	INTEGER,
	`created`	INTEGER NOT NULL,
	`edited`	INTEGER NOT NULL
);");

    }

    /**
     * @return OwnPDO
     */
    public function getSql ()
    {
        return $this->sql;
    }

    /**
     * @param OwnPDO $sql
     */
    public function setSql ($sql)
    {
        $this->sql = $sql;
    }


}
