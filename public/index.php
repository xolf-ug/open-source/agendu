<?php

require __DIR__.'/../vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['files'] = __DIR__.'/../files';

$app = new Slim\App(["settings" => $config]);

$container = $app->getContainer();
include __DIR__.'/../src/container.php';

include __DIR__.'/../src/route.php';

$app->run();
